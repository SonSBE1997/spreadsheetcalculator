package dev.sanero.spreadsheet.app;

import dev.sanero.spreadsheet.entity.Sheet;
import dev.sanero.spreadsheet.utils.Constants;
import dev.sanero.spreadsheet.utils.CustomException;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        long time = System.currentTimeMillis();

        Sheet sheet = new Sheet();
        boolean isReadDataSuccess = true;
        try {
            sheet.setCellMapFromFile("test.txt", true);
        } catch (IOException e) {
            System.out.println("An error has occurred while read data from file.");
            isReadDataSuccess = false;
        } catch (CustomException e) {
            if (Constants.WRONG_FILE_FORMAT.equals(e.getMessage())) {
                System.out.println("File format incorrect. Please check!");
            } else if (Constants.SHEET_INFO_INCORRECT.equals(e.getMessage())) {
                System.out.println("Sheet info invalid.");
            } else if (Constants.FILE_NOT_FOUND.equals(e.getMessage())) {
                System.out.println("File not found.");
            }
            isReadDataSuccess = false;
        } catch (Exception e) {
            System.out.println("An error has occurred while init sheet data.");
            isReadDataSuccess = false;
        }

        if (isReadDataSuccess) {
            boolean hasCyclicDependencies = sheet.detectCyclicDependencies();
            if (!hasCyclicDependencies) {
                sheet.calculateSheetData();
            }
        }

        System.out.println("Time: " + 0.1 * (System.currentTimeMillis() - time) / 1000);
    }
}
