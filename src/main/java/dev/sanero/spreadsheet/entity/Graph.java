package dev.sanero.spreadsheet.entity;

import java.util.*;

/**
 * Data structure: Graph
 *
 * @param <T>
 */
public class Graph<T> {

    private Map<T, List<T>> map = new HashMap<>();

    /**
     * add vertex of graph
     *
     * @param s
     */
    public void addVertex(T s) {
        map.put(s, new LinkedList<>());
    }

    /**
     * add edge between 2 vertices
     *
     * @param source:        vertex 1
     * @param destination:   vertex 2
     * @param bidirectional: edge has bi-directional or not.
     */
    public void addEdge(T source, T destination, boolean bidirectional) {
        if (!map.containsKey(source)) {
            addVertex(source);
        }

        if (!map.containsKey(destination)) {
            addVertex(destination);
        }

        map.get(source).add(destination);

        if (bidirectional) {
            map.get(destination).add(source);
        }
    }

    /**
     * number of vertices
     *
     * @return
     */
    public int getVertexCount() {
        return map.keySet().size();
    }

    /**
     * Number of edges
     *
     * @param bidirection
     */
    public int getEdgesCount(boolean bidirection) {
        int count = 0;
        for (T v : map.keySet()) {
            count += map.get(v).size();
        }
        if (bidirection) {
            count = count / 2;
        }
        return count;
    }

    /**
     * Check if graph has vertex
     *
     * @param s
     * @return
     */
    public boolean hasVertex(T s) {
        return map.containsKey(s);
    }

    /**
     * Check if graph has edge between 2 vertices
     *
     * @param s
     * @param d
     * @return
     */
    public boolean hasEdge(T s, T d) {
        return map.get(s).contains(d);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (T v : map.keySet()) {
            builder.append(v.toString() + ": ");
            for (T w : map.get(v)) {
                builder.append(w.toString() + " ");
            }
            builder.append("\n");
        }

        return (builder.toString());
    }

    /**
     * Reset graph
     */
    public void resetGraph() {
        map.clear();
    }

    /**
     * Detect cyclic dependencies and save each cyclic as a list member
     *
     * @return
     */
    public List<List<T>> dfs() {
        Map<T, Integer> visited = new HashMap<>();
        for (T key : map.keySet()) {
            visited.put(key, -1);
        }
        Stack<T> pathStack = new Stack<>();
        List<List<T>> circularList = new ArrayList<>();
        for (T vertex : map.keySet()) {
            if (visited.get(vertex) == -1) {
                pathStack.clear();
                pathStack.push(vertex);
                visited.replace(vertex, 0);
                processDFSTree(pathStack, visited, circularList);
            }
        }
        return circularList;
    }

    /**
     * process graph tree
     *
     * @param pathStack
     * @param visited:     -1: not visited, 0: in-stack, 1: done
     * @param circularList
     */
    private void processDFSTree(Stack<T> pathStack, Map<T, Integer> visited, List<List<T>> circularList) {
        T top = pathStack.peek();
        Iterator<T> nextVertexIterator = map.get(top).iterator();
        T v;
        while (nextVertexIterator.hasNext()) {
            v = nextVertexIterator.next();
            if (visited.get(v) == 0) {
                List<T> circularMember = new ArrayList<>();
                saveCyclicDependency(pathStack, v, circularMember);
                if (!circularMember.isEmpty()) {
                    circularList.add(circularMember);
                }
            } else if (visited.get(v) == -1) {
                pathStack.push(v);
                visited.replace(v, 0);
                processDFSTree(pathStack, visited, circularList);
            }
        }
        visited.replace(top, 1);
        pathStack.pop();
    }

    /**
     * detect cyclic dependency member from vertex v and store it into circularMember
     *
     * @param pathStack
     * @param v
     * @param circularMember
     */
    private void saveCyclicDependency(Stack<T> pathStack, T v, List<T> circularMember) {
        Stack<T> tempStack = new Stack<>();
        tempStack.push(pathStack.pop());
        while (!tempStack.peek().equals(v)) {
            tempStack.push(pathStack.pop());
        }

        while (!tempStack.isEmpty()) {
            circularMember.add(tempStack.peek());
            pathStack.push(tempStack.pop());
        }
    }
}