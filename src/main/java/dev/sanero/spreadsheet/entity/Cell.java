package dev.sanero.spreadsheet.entity;

import dev.sanero.spreadsheet.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Cell info
 */
public class Cell {
    private List<String> referenceCells;
    private String expression;
    private String data;
    private String label;

    public Cell(String label, String expression) {
        this.label = label;
        this.expression = expression;
        this.data = "";
        getReferenceCellsFromExpression();
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public List<String> getReferenceCells() {
        return referenceCells;
    }

    public void setReferenceCells(List<String> referenceCells) {
        this.referenceCells = referenceCells;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private void getReferenceCellsFromExpression() {
        referenceCells = new ArrayList<>();
        if ("".equals(expression)) {
            return;
        }
        String[] opers = expression.split(" ");
        for (String oper : opers) {
            if (!Utils.isNumeric(oper) && !Utils.isOperator(oper)) {
                referenceCells.add(oper);
            }
        }
    }

}
