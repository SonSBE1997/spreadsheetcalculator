package dev.sanero.spreadsheet.entity;

import dev.sanero.spreadsheet.utils.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Spreadsheet entity
 */
public class Sheet {
    private Map<String, Cell> cellMap;
    private Graph<String> graph;
    private int row, col;
    private List<String> labelList;

    public Sheet() {
        cellMap = new HashMap<>();
        graph = new Graph<>();
        labelList = new ArrayList<>();
    }

    /**
     * Read data from file and set it into spreadsheet
     *
     * @param filePath
     * @param isUnderClassPath
     * @throws Exception
     */
    public void setCellMapFromFile(String filePath, boolean isUnderClassPath) throws IOException, CustomException {
        Queue<String> dataQueue;
        try {
            dataQueue = Files.readFile(filePath, isUnderClassPath);
        } catch (NullPointerException | FileNotFoundException ex) {
            throw new CustomException(Constants.FILE_NOT_FOUND);
        }

        String sheetMatrixInfo = dataQueue.poll();
        if (sheetMatrixInfo == null) {
            throw new CustomException(Constants.WRONG_FILE_FORMAT);
        }

        String[] matrix = sheetMatrixInfo.split(" ");
        if (matrix.length != 2) {
            throw new CustomException(Constants.SHEET_INFO_INCORRECT);
        }
        try {
            col = Integer.parseInt(matrix[0]);
            row = Integer.parseInt(matrix[1]);
        } catch (NumberFormatException e) {
            throw new CustomException(Constants.SHEET_INFO_INCORRECT);
        }

        if (row > 26 || row < 1 || col < 1) {
            throw new CustomException(Constants.SHEET_INFO_INCORRECT);
        }

        // init cell data
        cellMap.clear();
        labelList.clear();
        for (int i = 0; i < row; ++i) {
            for (int j = 1; j <= col; ++j) {
                String label = (char) (i + 65) + "" + j;
                String cellExpression = dataQueue.poll();
                if (cellExpression == null) {
                    throw new CustomException(Constants.WRONG_FILE_FORMAT);
                }
                cellMap.put(label, new Cell(label, cellExpression));
                labelList.add(label);
            }
        }

        // init graph data
        graph.resetGraph();
        for (String key : cellMap.keySet()) {
            for (String referenceKey : cellMap.get(key).getReferenceCells()) {
                graph.addEdge(referenceKey, key, false);
            }
        }
    }

    /**
     * Detect cyclic dependencies and report these
     *
     * @return spreadsheet has cyclic dependencies or not
     */
    public boolean detectCyclicDependencies() {
        List<List<String>> cyclicDependencies = graph.dfs();
        List<String> ownReference = new ArrayList<>();
        StringBuilder cyclicPaths = new StringBuilder();
        if (cyclicDependencies.size() > 0) {
            for (List<String> cyclicVertex : cyclicDependencies) {
                if (cyclicVertex.size() == 1) { // if cell reference to itself
                    ownReference.add(cyclicVertex.get(0));
                } else {
                    cyclicPaths.append(cyclicVertex.stream().collect(Collectors.joining("->"))).append("\r\n");
                }
            }
            if (ownReference.size() > 0) {
                System.out.println("There are one or more own references: " + ownReference.stream().collect(Collectors.joining(", ")));
            }
            if (cyclicPaths.length() > 0) {
                System.out.println("There are one or more circular references: ");
                System.out.println(cyclicPaths);
            }
            return true;
        }
        return false;
    }

    /**
     * Calculate all cell data of spreadsheet
     */
    public void calculateSheetData() {
        for (String key : cellMap.keySet()) {
            calculateCellData(key);
        }
        printSheetData();
    }

    /**
     * Print all data of spreadsheet
     */
    public void printSheetData() {
        System.out.println(String.format("%d %d", col, row));
        for (String cell : labelList) {
            String cellData = cellMap.get(cell).getData();
            if (Utils.isNumeric(cellData)) {
                System.out.println(Utils.formatNumber5DecimalPlace(Double.valueOf(cellData)));
            } else {
                System.out.println(cellData);
            }
        }
    }

    /**
     * Calculate cell data
     *
     * @param key
     * @return
     */
    public String calculateCellData(String key) {
        Cell cell = cellMap.get(key);

        if (Utils.isNumeric(cell.getData()) || Constants.DIV_BY_ZERO.equals(cell.getData()) || Constants.WRONG_FORMULAR.equals(cell.getData())) {
            return cell.getData();
        }

        String expression = cell.getExpression();
        for (String refeCell : cell.getReferenceCells()) {
            expression = expression.replace(refeCell, calculateCellData(refeCell)); // recursive
        }

        if (expression.contains(Constants.WRONG_FORMULAR)) {
            cell.setData(Constants.WRONG_FORMULAR);
        } else if (expression.contains(Constants.DIV_BY_ZERO)) {
            cell.setData(Constants.DIV_BY_ZERO);
        } else {
            try {
                cell.setData(Calculator.calculateRPNExpression(expression) + "");
            } catch (NumberFormatException e) {
                cell.setData(Constants.WRONG_FORMULAR);
            } catch (CustomException e) {
                if (Constants.DIV_BY_ZERO.equals(e.getMessage())) {
                    cell.setData(Constants.DIV_BY_ZERO);
                } else {
                    cell.setData(Constants.WRONG_FORMULAR);
                }
            }
        }

        return cell.getData();
    }
}
