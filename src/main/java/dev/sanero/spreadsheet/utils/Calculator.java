package dev.sanero.spreadsheet.utils;

import java.util.Stack;

public class Calculator {
    /**
     * calculate an RPN expression (RPN: Reverse Polish Notation)
     *
     * @param expression
     * @return
     */
    public static double calculateRPNExpression(String expression) throws CustomException {
        String[] opers = expression.split(" ");
        if (opers.length == 1) {
            if ("".equals(opers[0])) {
                opers[0] = "0";
            }
            return Double.parseDouble(opers[0]);
        }
        Stack<Double> operandStack = new Stack<>();
        for (int i = 0; i < opers.length; ++i) {
            if (Utils.isOperator(opers[i])) {
                double operand2 = operandStack.pop();
                double operand1 = operandStack.pop();
                double result = 0;
                if ("+".equals(opers[i])) {
                    result = operand1 + operand2;
                } else if ("-".equals(opers[i])) {
                    result = operand1 - operand2;
                } else if ("*".equals(opers[i])) {
                    result = operand1 * operand2;
                } else if ("/".equals(opers[i])) {
                    if (operand2 == 0) {
                        throw new CustomException(Constants.DIV_BY_ZERO);
                    }
                    result = operand1 / operand2;
                }
                operandStack.push(result);
            } else {
                operandStack.push(Double.parseDouble(opers[i]));

            }
        }
        if (operandStack.isEmpty() || operandStack.size() > 1) {
            throw new CustomException(Constants.WRONG_FORMULAR);
        }
        return operandStack.pop();
    }
}
