package dev.sanero.spreadsheet.utils;

public class Constants {
    public static final String DIV_BY_ZERO = "#DIV/0!";
    public static final String WRONG_FORMULAR = "#NAME?";
    public static final String WRONG_FILE_FORMAT = "WRONG FILE FORMAT";
    public static final String FILE_NOT_FOUND = "FILE NOT FOUND";
    public static final String SHEET_INFO_INCORRECT = "SHEET INFO INCORRECT";
}
