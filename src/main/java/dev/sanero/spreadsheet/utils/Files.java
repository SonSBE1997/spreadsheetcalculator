package dev.sanero.spreadsheet.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Queue;

public class Files {
    /**
     * Read text document file and reduce a queue
     *
     * @param filePath
     * @param isUnderClassPath
     * @return
     */
    public static Queue<String> readFile(String filePath, boolean isUnderClassPath) throws NullPointerException, IOException {
        Queue<String> dataQueue = new ArrayDeque<>();
        InputStream is;
        if (isUnderClassPath) {
            is = Files.class.getClassLoader().getResourceAsStream(filePath);
        } else {
            is = new FileInputStream(new File(filePath));
        }
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        String line;
        while ((line = reader.readLine()) != null) {
            dataQueue.add(line);
        }
        return dataQueue;
    }
}
