package dev.sanero.spreadsheet.utils;

import java.text.DecimalFormat;

public class Utils {
    /**
     * Check if a string contain characters
     *
     * @param str
     * @return
     */
    public static boolean isContainCharacter(String str) {
        return str.matches("(.)*([a-zA-Z])(.)*");
    }

    /**
     * Check if a string is a number
     *
     * @param numb
     * @return
     */
    public static boolean isNumeric(String numb) {
        return numb.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Format number with 5 decimal place, trailing with zero
     *
     * @param value
     * @return
     */
    public static String formatNumber5DecimalPlace(double value) {
        DecimalFormat df = new DecimalFormat("0.00000");
        return df.format(value);
    }

    /**
     * Round number to n decimal places
     *
     * @param value:  numerical value
     * @param places: number of decimal places
     * @return rounded number as string
     */
    public static String formatNumber(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale + "";
    }

    /**
     * Check if a string is an operator (+, -, *, /)
     *
     * @param oper: operand or operator
     * @return
     */
    public static boolean isOperator(String oper) {
        return "+".equals(oper) || "-".equals(oper) || "*".equals(oper) || "/".equals(oper);
    }
}
